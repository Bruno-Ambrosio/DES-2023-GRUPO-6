package com.shashi.srv;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.shashi.beans.DemandBean;
import com.shashi.beans.ProductBean;
import com.shashi.service.impl.CartServiceImpl;
import com.shashi.service.impl.DemandServiceImpl;
import com.shashi.service.impl.ProductServiceImpl;
import com.shashi.service.impl.UserServiceImpl;

/**
 * Servlet implementation class AddtoCart
 */
@WebServlet("/RemoveProductToFavorite")
public class RemoveProductToFavorite extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public RemoveProductToFavorite() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();
		String userName = (String) session.getAttribute("username");
		String password = (String) session.getAttribute("password");
		String usertype = (String) session.getAttribute("usertype");
		if (userName == null || password == null || usertype == null || !usertype.equalsIgnoreCase("customer")) {
			response.sendRedirect("login.jsp?message=Session Expired, Login Again to Continue!");
			return;
		}

		// login Check Successfull

		String userId = userName;
		String prodId = request.getParameter("pid");

		CartServiceImpl cart = new CartServiceImpl();

		ProductServiceImpl productDao = new ProductServiceImpl();

		ProductBean product = productDao.getProductDetails(prodId);

		PrintWriter pw = response.getWriter();

		response.setContentType("text/html");

		UserServiceImpl userDao = new UserServiceImpl();

		userDao.removeProductToFavorite(userId, prodId);

		String status = "Product Removed From Favorite Successfully!";
		
		RequestDispatcher rd = request.getRequestDispatcher("userHome.jsp");

		rd.include(request, response);
		
		String scripts = "";
		scripts = scripts.concat("<script>alert('" + status + "')</script>");
		scripts = scripts.concat("<script>window.location.href = '/shopping-cart';</script>");

		pw.println(scripts);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		doGet(request, response);
	}

}
