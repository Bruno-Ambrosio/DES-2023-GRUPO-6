/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.shashi.beans;

/**
 *
 * @author rgeal
 */

@SuppressWarnings("serial")
public class FavoriteBean {
 
    public String userId;
    public String productId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public FavoriteBean() {
    }

    public FavoriteBean(String userId, String productId) {
        this.userId = userId;
        this.productId = productId;
    }
}
