FROM docker.io/library/maven:latest AS builder

WORKDIR /app

COPY . /app

RUN mvn clean package

FROM docker.io/library/tomcat:9.0.8

COPY conf/server.xml /usr/local/tomcat/conf/

COPY --from=builder app/target/*.war /usr/local/tomcat/webapps/ROOT.war

RUN rm -rf /usr/local/tomcat/webapps/ROOT

EXPOSE 8080 8017

CMD ["catalina.sh", "run"]