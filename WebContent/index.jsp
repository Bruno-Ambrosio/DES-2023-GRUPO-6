<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<%@ page
    import="com.shashi.service.impl.*, com.shashi.service.*,com.shashi.beans.*,java.util.*,javax.servlet.ServletOutputStream,java.io.*"%>
    <!DOCTYPE html>
    <html>
        <head>
            <title>Ellison Electronics</title>
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <link rel="stylesheet"
                  href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
            <link rel="stylesheet" href="css/changes.css">
            <link rel="stylesheet" href="css/card.css">
            <script
            src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
            <script
            src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
        </head>
        <body style="background-color: #E6F9E6;">

            <%
                /* Checking the user credentials */
                String userName = (String) session.getAttribute("username");
                String password = (String) session.getAttribute("password");
                String userType = (String) session.getAttribute("usertype");

                boolean isValidUser = true;

                if (userType == null || userName == null || password == null || !userType.equals("customer")) {

                    isValidUser = false;
                }

                ProductServiceImpl prodDao = new ProductServiceImpl();
                List<ProductBean> products = new ArrayList<ProductBean>();
                String message = "All Products";

                try {

                    List<String> tipo = new ArrayList<String>();
                    String menor = request.getParameter("menorpreco");
                    String maior = request.getParameter("maiorpreco");
                    String filtrar = request.getParameter("filtrar");
                    String nome = request.getParameter("nome");
                    String avaliacao = request.getParameter("aval");
                    String marca = request.getParameter("marca");
                    String pagina = request.getParameter("page");
                    tipo.add(request.getParameter("mobile"));
                    tipo.add(request.getParameter("tv"));
                    tipo.add(request.getParameter("laptop"));
                    tipo.add(request.getParameter("camera"));
                    tipo.add(request.getParameter("speaker"));
                    tipo.add(request.getParameter("tablet"));

                    if (filtrar != null) {
                        message = "Exibindo produtos filtrados - P�GINA �NICA";
                        products = prodDao.filtragemAmpla(menor, maior, tipo, nome, avaliacao, marca, "-1");
                    } else {
                        String pa = "";
                        if (pagina == null) pa = "1"; else pa = pagina;
                        message = "Todos os produtos - P�GINA " + pa;
                        products = prodDao.filtragemAmpla(null, null, null, null, null, null, pagina);
                    }

                } catch (IllegalArgumentException er) {
                    response.sendRedirect(request.getContextPath() + "/index.jsp");
                }

            %>
            <jsp:include page="header.jsp" />

            <script type="text/javascript">
                async function useRequest(favorited, userid, prodid) {
                    let url = '';
                    if (favorited) {
                        url = './AddProductToFavorite?uid=' + userid + '&pid=' + prodid;
                    } else {
                        url = './RemoveProductToFavorite?uid=' + userid + '&pid=' + prodid;
                    }

                    const resource = fetch(url, {
                        method: 'POST',
                        mode: 'cors'
                    }).then(function (response) {
                        return response;
                    }).then(function (data) {
                        return data;
                    }).catch(function (e) {
//                        alert('Erro no servidor');
                    });

                    cosnole.log('resource', resource);
                }
                async function removerDosFavoritos(userid, prodid) {
                    const result = await useRequest(false, userid, prodid);
                }
                async function adicionarAosFavoritos(userid, prodid) {
                    const result = await useRequest(true, userid, prodid);
                }
            </script>

            <div class="text-center"
                 style="color: black; font-size: 14px; font-weight: bold; margin: 8px"><%=message%></div>
            <!-- Start of Product Items List -->
            <div class="container">
                <div class="row text-center">

                    <%
                        for (ProductBean product : products) {
                            int cartQty = new CartServiceImpl().getCartItemCount(userName, product.getProdId());
                    %>
                    <div class="col-sm-4" style='height: 350px;'>
                        <div class="thumbnail">
                            
                            <form method="post">
                                <%
                                    if (!product.isFavorited()) {
                                %>
                                <button class="add-to-favorite" onclick="adicionarAosFavoritos('<%=userName%>', '<%=product.getProdId()%>')">
                                    <i class="fa fa-heart text-dark" style="color:black"></i>
                                </button>
                                <%
                                } else {
                                %>
                                <button class="add-to-favorite" onclick="removerDosFavoritos('<%=userName%>', '<%=product.getProdId()%>')">
                                    <i class="fa fa-heart text-danger" style="color:red"></i>
                                </button>
                                <%
                                    }
                                %>
                            </form>

                            <img src="./ShowImage?pid=<%=product.getProdId()%>" alt="Product"
                                 style="height: 150px; max-width: 180px">
                            <p class="productname"><%=product.getProdName()%>
                            </p>
                            <%
                                String description = product.getProdInfo();
                                description = description.substring(0, Math.min(description.length(), 100));
                            %>
                            <p class="productinfo"><%=description%>..
                            </p>
                            <p class="price">
                                Rs
                                <%=product.getProdPrice()%>
                            </p>
                            <form method="post">
                                <%
                                    if (cartQty == 0) {
                                %>
                                <button type="submit"
                                        formaction="./AddtoCart?uid=<%=userName%>&pid=<%=product.getProdId()%>&pqty=1"
                                        class="btn btn-success">Add to Cart</button>
                                &nbsp;&nbsp;&nbsp;
                                <button type="submit"
                                        formaction="./AddtoCart?uid=<%=userName%>&pid=<%=product.getProdId()%>&pqty=1"
                                        class="btn btn-primary">Buy Now</button>
                                <%
                                } else {
                                %>
                                <button type="submit"
                                        formaction="./AddtoCart?uid=<%=userName%>&pid=<%=product.getProdId()%>&pqty=0"
                                        class="btn btn-danger">Remove From Cart</button>
                                &nbsp;&nbsp;&nbsp;
                                <button type="submit" formaction="cartDetails.jsp"
                                        class="btn btn-success">Checkout</button>
                                <%
                                    }
                                %>
                            </form>
                            <br />
                        </div>
                    </div>

                    <%
                        }
                    %>

                </div>

                <nav>
                    <ul class="pagination">
                        <p><b>Pagina��o de todos os produtos</b></p>
                        <li class=${filtro}><a href="index.jsp?page=1" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>
                            <%
                                List<ProductBean> produtos = prodDao.getAllProducts();
                                int i = 1;
                                int numero = produtos.size() - 1;
                                double numeroPag = numero / 9;
                                int numeroPags = 0;
                                if ((numeroPag / (int) numeroPag) == 1.0) {
                                    numeroPags = (int) numeroPag;
                                } else {
                                    numeroPags = (int) numeroPag + 1;
                                }
                                for (; i <= numeroPags + 1; i++) {
                            %>
                            <li class="enabled"><a href="index.jsp?page=<%=i%>"><%=i%> <span class="sr-only">(current)</span></a></li>
                        <%
                            }
                        %>
                    </ul>
                </nav>

            </div>
            <!-- ENd of Product Items List -->


            <%@ include file="footer.html"%>

        </body>
    </html>