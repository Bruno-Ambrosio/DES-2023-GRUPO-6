<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<%@ page import="com.shashi.service.impl.*, com.shashi.service.*"%>
<%
    /* Checking the user credentials */
    String userType = (String) session.getAttribute("usertype");
%>
<!DOCTYPE html>
<html>
    <head>
        <title>Logout Header</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet"
              href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="css/changes.css">
        <script
        src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script
        src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
        <link rel="stylesheet"
              href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    </head>
    <body style="background-color: #E6F9E6;">
        <!--Company Header Starting  -->
        <div class="container-fluid text-center"
             style="margin-top: 45px; background-color: #33cc33; padding: 0px;">
            <h2 style="Color: #FFFF">Ellison Electronics</h2>
            <h6 style="Color:#FFFF">We specialize in Electronics</h6>

            <div style="background-color: #45FF96;
                 color: #000000;
                 box-shadow: 5px 3px 7px #000000; padding: 16px; display: flex">
                <% if (userType == null) { %>
                <form style="display: flex; Color:#000000 " class="container-fluid text-center" action="index.jsp" method="get">
                    <% } else { %>
                    <form style="display: flex; Color:#000000 " class="container-fluid text-center" action="userHome.jsp" method="get">
                        <% } %>
                        <div style="text-align: center; margin:0 30px; width: 100%" class="form-group">
                            <input placeholder="Nome do Produto" style="margin:0 5px" type="input" name="nome" type="text" maxlength="48" size="30"/>
                            <input placeholder="Valor min.." style="margin:0 5px" type="input" name="menorpreco" maxlength="10" pattern="[0-9]*"/>
                            <input placeholder="Valor max.." style="margin:0 5px" type="input" name="maiorpreco" maxlength="10" pattern="[0-9]*"/>
                        </div>
                        <div style="display: inline-flexbox; box-shadow: 5px 3px 7px #000000; margin:10px 0; padding:0 12px" name="tipoProduto">
                            <input type="checkbox" name="mobile" value="mobile" /> Mobile
                            <input type="checkbox" name="tv" value="tv" /> TV
                            <input type="checkbox" name="laptop" value="laptop" /> Laptop
                            <input type="checkbox" name="camera" value="camera" /> Camera
                            <input type="checkbox" name="speaker" value="speaker" /> Speaker
                            <input type="checkbox" name="tablet" value="tablet" /> Tablet
                        </div>
                        <div style="display: flexbox; padding: 0 40px;">
                            <div>
                                <label>Avaliacao</label>
                            </div>
                            <select class="form-control" name="aval">
                                <option>TODOS</option>
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                                <option>5</option>
                            </select>
                        </div>
                        <div style="display: flexbox; padding: 0 8px;">
                            <div>
                                <label>Marca</label>
                            </div>
                            <select class="form-control" name="marca">
                                <option>TODOS</option>
                                <option>ACER</option>
                                <option>APPLE</option>
                                <option>ASUS</option>
                                <option>BAJAJ</option>
                                <option>CANON</option>
                                <option>GOOGLE</option>
                                <option>HP</option>
                                <option>IMPLY</option>
                                <option>INDUSBAY</option>
                                <option>INFINIX</option>
                                <option>JBL</option>
                                <option>KENSTAR</option>
                                <option>LENOVO</option>
                                <option>MIVI</option>
                                <option>MOTOROLA</option>
                                <option>REALME</option>
                                <option>SAMSUNG</option>
                                <option>XIAOMI</option>
                                <option>ZEBRONICS</option>
                            </select>
                        </div>
                        <input style="margin:0 5px" type="submit" class="btn btn-primary" value="Filtrar" name="filtrar"/>
                    </form>
            </div>
            <p align="center"
               style="color: blue; font-weight: bold; margin-top: 5px; margin-bottom: 5px;"
               id="message"></p>
        </div>
        <!-- Company Header Ending -->
        <%
            /* Checking the user credentials */
            if (userType == null) { //LOGGED OUT
        %>
        <!-- Starting Navigation Bar -->
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse"
                            data-target="#myNavbar">
                        <span class="icon-bar"></span> <span class="icon-bar"></span> <span
                            class="icon-bar"></span>
                    </button>
                    <% if (userType == null) { %>
                    <a class="navbar-brand" href="index.jsp">
                    <% } else { %>
                    <a class="navbar-brand" href="userHome.jsp">
                    <% } %><span
                            class="glyphicon glyphicon-home">&nbsp;</span>Shopping Center</a>
                </div>
                <div class="collapse navbar-collapse" id="myNavbar">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="login.jsp">Login</a></li>
                            <% if (userType != null) { %>
                        <li><a href="userFavorite.jsp">Favorites</a></li>
                            <% } %>
                        <li><a href="register.jsp">Register</a></li>
                        <li><a href="index.jsp">Products</a></li>
                        <li class="dropdown"><a class="dropdown-toggle"
                                                data-toggle="dropdown" href="#">Category <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="index.jsp?type=mobile">Mobiles</a></li>
                                <li><a href="index.jsp?type=tv">TVs</a></li>
                                <li><a href="index.jsp?type=laptop">Laptops</a></li>
                                <li><a href="index.jsp?type=camera">Camera</a></li>
                                <li><a href="index.jsp?type=speaker">Speakers</a></li>
                                <li><a href="index.jsp?type=tablet">Tablets</a></li>
                            </ul></li>
                    </ul>
                </div>
            </div>
        </nav>
        <%
        } else if ("customer".equalsIgnoreCase(userType)) { //CUSTOMER HEADER
        %>
        <!-- Starting Navigation Bar -->
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse"
                            data-target="#myNavbar">
                        <span class="icon-bar"></span> <span class="icon-bar"></span> <span
                            class="icon-bar"></span>
                    </button>
                    <% if (userType == null) { %>
                    <a class="navbar-brand" href="index.jsp">
                    <% } else { %>
                    <a class="navbar-brand" href="userHome.jsp">
                    <% } %><span
                            class="glyphicon glyphicon-home">&nbsp;</span>Shopping Center</a>
                </div>
                <div class="collapse navbar-collapse" id="myNavbar">
                    <ul class="nav navbar-nav navbar-right">
                        <% if (userType != null) { %>
                        <li>
                            <form action="./LogoutSrv" method="POST">
                                <button style="appearance: none" type="submit">Logout</button>
                            </form>
                        </li>
                        <% } else { %>
                        <li><a href="login.jsp">Login</a></li>
                            <% } %>
                            <% if (userType != null) { %>
                        <li><a href="userFavorite.jsp">Favorites</a></li>
                            <% } %>
                        <li><a href="register.jsp">Register</a></li>
                        <li><a href="index.jsp">Products</a></li>
                        <li class="dropdown"><a class="dropdown-toggle"
                                                data-toggle="dropdown" href="#">Category <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="index.jsp?type=mobile">Mobiles</a></li>
                                <li><a href="index.jsp?type=tv">TVs</a></li>
                                <li><a href="index.jsp?type=laptop">Laptops</a></li>
                                <li><a href="index.jsp?type=camera">Camera</a></li>
                                <li><a href="index.jsp?type=speaker">Speakers</a></li>
                                <li><a href="index.jsp?type=tablet">Tablets</a></li>
                            </ul></li>
                    </ul>
                </div>
            </div>
        </nav>
        <%
        } else if ("customer".equalsIgnoreCase(userType)) { //CUSTOMER HEADER

            int notf = new CartServiceImpl().getCartCount((String) session.getAttribute("username"));
        %>
        <nav class="navbar navbar-default navbar-fixed-top">

            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse"
                            data-target="#myNavbar">
                        <span class="icon-bar"></span> <span class="icon-bar"></span> <span
                            class="icon-bar"></span>
                    </button>
                    <% if (userType == null) { %>
                    <a class="navbar-brand" href="index.jsp">
                    <% } else { %>
                    <a class="navbar-brand" href="userHome.jsp">
                    <% } %><span
                            class="glyphicon glyphicon-home">&nbsp;</span>Shopping Center</a>
                </div>
                <div class="collapse navbar-collapse" id="myNavbar">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="userHome.jsp"><span
                                    class="glyphicon glyphicon-home">Products</span></a></li>
                        <li class="dropdown"><a class="dropdown-toggle"
                                                data-toggle="dropdown" href="#">Category <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="userHome.jsp?type=mobile">Mobiles</a></li>
                                <li><a href="userHome.jsp?type=tv">TV</a></li>
                                <li><a href="userHome.jsp?type=laptop">Laptops</a></li>
                                <li><a href="userHome.jsp?type=camera">Camera</a></li>
                                <li><a href="userHome.jsp?type=speaker">Speakers</a></li>
                                <li><a href="userHome.jsp?type=tablet">Tablets</a></li>
                            </ul></li>
                            <%
                                if (notf == 0) {
                            %>
                        <li><a href="cartDetails.jsp"> <span
                                    class="glyphicon glyphicon-shopping-cart"></span>Cart
                            </a></li>

                        <%
                        } else {
                        %>
                        <li><a href="cartDetails.jsp"
                               style="margin: 0px; padding: 0px;" id="mycart"><i
                                    data-count="<%=notf%>"
                                    class="fa fa-shopping-cart fa-3x icon-white badge"
                                    style="background-color: #333; margin: 0px; padding: 0px; padding-bottom: 0px; padding-top: 5px;">
                                </i></a></li>
                                <%
                                    }
                                %>
                        <li><a href="orderDetails.jsp">Orders</a></li>
                        <li><a href="userProfile.jsp">Profile</a></li>
                        <li><a href="./LogoutSrv">Logout</a></li>
                    </ul>
                </div>
            </div>
        </nav>
        <%
        } else { //ADMIN HEADER
        %>
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse"
                            data-target="#myNavbar">
                        <span class="icon-bar"></span> <span class="icon-bar"></span> <span
                            class="icon-bar"></span>
                    </button>
                    <% if (userType == null) { %>
                    <a class="navbar-brand" href="index.jsp">
                    <% } else { %>
                    <a class="navbar-brand" href="userHome.jsp">
                    <% } %>
                        <span class="glyphicon glyphicon-home">&nbsp;</span>Shopping Center</a>
                </div>
                <div class="collapse navbar-collapse" id="myNavbar">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="adminViewProduct.jsp">Products</a></li>
                        <li class="dropdown"><a class="dropdown-toggle"
                                                data-toggle="dropdown" href="#">Category <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="adminViewProduct.jsp?type=mobile">Mobiles</a></li>
                                <li><a href="adminViewProduct.jsp?type=tv">Tvs</a></li>
                                <li><a href="adminViewProduct.jsp?type=laptop">Laptops</a></li>
                                <li><a href="adminViewProduct.jsp?type=camera">Camera</a></li>
                                <li><a href="adminViewProduct.jsp?type=speaker">Speakers</a></li>
                                <li><a href="adminViewProduct.jsp?type=tablet">Tablets</a></li>
                            </ul></li>
                        <li><a href="adminStock.jsp">Stock</a></li>
                        <li><a href="shippedItems.jsp">Shipped</a></li>
                        <li><a href="unshippedItems.jsp">Orders</a></li>
                        <!-- <li><a href=""> <span class="glyphicon glyphicon-shopping-cart"></span>&nbsp;Cart</a></li> -->
                        <li class="dropdown"><a class="dropdown-toggle"
                                                data-toggle="dropdown" href="#">Update Items <span
                                    class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="addProduct.jsp">Add Product</a></li>
                                <li><a href="removeProduct.jsp">Remove Product</a></li>
                                <li><a href="updateProductById.jsp">Update Product</a></li>
                            </ul></li>
                        <li><a href="./LogoutSrv">Logout</a></li>

                    </ul>
                </div>
            </div>
        </nav>
        <%
            }
        %>
        <!-- End of Navigation Bar -->
    </body>
</html>